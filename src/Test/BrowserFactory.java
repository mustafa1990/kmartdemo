package Test;
/**
 * Functional Decomposition Framework
 *  Mahesh NVS 30/5/2016
 *
 * 
 */
import java.io.File;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class BrowserFactory {
	public static WebDriver browser = null;
	
	public static WebDriver getBrowser(String browserName) {

		if (browser == null) {
			try {

				
//				String browserName = CommonUtils.readFromConfig("Browser");

				if ("FF".equalsIgnoreCase(browserName)) {
					
					browser = loadFireFoxDriver();
				} else if ("IE".equalsIgnoreCase(browserName)) {
					browser = loadIEDriver();
				} else if ("chrome".equalsIgnoreCase(browserName)) {
					browser = loadChromeDriver();					
				}	
				else if("edge".equalsIgnoreCase(browserName)){				
					browser = loadEdgeDriver();
				}
				else if("opera".equalsIgnoreCase(browserName)){
					browser =loadOperaDriver();
				
				}
			} catch (Exception exception) {
				System.out.println("Error Occured");
			}

		}

		return browser;
	}

	private static RemoteWebDriver loadEdgeDriver() throws Exception 
	{
		RemoteWebDriver remoteDriver = null;
		
		System.out.println("launching Microsoft Edge browser");
		System.setProperty("webdriver.edge.driver", "C:\\CI_CD_CT\\Browser_Executables\\MicrosoftWebDriver.exe");
		browser = new EdgeDriver();
		
		
		
		return remoteDriver;
	}
	
	private static RemoteWebDriver loadFireFoxDriver() throws Exception {

		String loadffProfile = CommonUtils.readFromConfig("loadffProfile");
		RemoteWebDriver remoteDriver = null;
		FirefoxProfile profile = null;
		

		if ("true".equalsIgnoreCase(loadffProfile)) {
			String profilePath = CommonUtils
					.readFromConfig("FIREFOXPROFILEDIR");
			File profileDir = new File(profilePath);
			profile = new FirefoxProfile(profileDir);
			profile.setAcceptUntrustedCertificates(false);
		
		}

		if ("true".equalsIgnoreCase(loadffProfile)) {
			remoteDriver = new FirefoxDriver(profile);
		} else {
			DesiredCapabilities ds = new DesiredCapabilities();
			ds.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
			System.setProperty("webdriver.gecko.driver", "C:\\CI_CD_CT\\Browser_Executables\\geckodriver.exe");
			browser  = new FirefoxDriver(ds);	
		    CC_SignIn chkLogin = new CC_SignIn();
		    chkLogin.chk_Login();
		   
		}
		return remoteDriver;

	}

	
	private static RemoteWebDriver loadIEDriver() throws Exception {

		RemoteWebDriver remoteDriver = null;
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
		System.setProperty("webdriver.ie.driver","C:\\CI_CD_CT\\Browser_Executables\\IEDriverServer.exe");
		browser = new InternetExplorerDriver(capabilities);
		Thread.sleep(9000);
		CC_SignIn chkLogin = new CC_SignIn();
	    chkLogin.chk_Login();
		
		return remoteDriver;

	}

	

	private static RemoteWebDriver loadChromeDriver() throws Exception {

		RemoteWebDriver remoteDriver = null;
		String hostOS = CommonUtils.getHostOperatingSystem();

		if (hostOS.equalsIgnoreCase("Mac OS X")) {
			System.setProperty("webdriver.chrome.driver","src/main/resources/browser_exe/chrome/chromedriver");
		} else {
			System.setProperty("webdriver.chrome.driver","C:\\CI_CD_CT\\Browser_Executables\\chromedriver.exe");
		}		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(ChromeOptions.CAPABILITY, options); 
		browser = new ChromeDriver();
		
		CC_SignIn chkLogin = new CC_SignIn();
	    chkLogin.chk_Login();
		return remoteDriver;
	}

	private static RemoteWebDriver loadOperaDriver() throws Exception
	{
		RemoteWebDriver remoteDriver = null;
		System.setProperty("webdriver.opera.driver","C:\\CI_CD_CT\\Browser_Executables\\operadriver_win64\\operadriver.exe");
//		remoteDriver = new OperaDriver();
		browser = new OperaDriver();		
		CC_SignIn chkLogin = new CC_SignIn();
	    chkLogin.chk_Login();
		
		return remoteDriver;
		
	}

}
