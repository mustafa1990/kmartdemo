package Test;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.log4testng.Logger;

public class CC_SignIn 
{
	Browser br = new Browser();
	public static Logger log = Logger.getLogger(CC_SignIn.class);
  public void chk_Login() throws Exception
  {
	  	String URL = CommonUtils.readFromConfig("BaseURL");
//		Browser br = new Browser();
		br.go(URL);	    
//		br.navigateToUrl(URL);
		String appTitle = br.getTitle();
		System.out.println("Application title is : "+appTitle);
		
		if(br.isDisplayed("xpath=//*[@id='Header_GlobalLogin_signInQuickLink']"))
		{
			System.out.println("Sign IN link is Displayed.Clicking on it");
			br.click("xpath=//*[@id='Header_GlobalLogin_signInQuickLink']");
		}
		else
		{
			System.out.println("Sign IN Link is NOT displayed");
		}
		
		Thread.sleep(3000);
		chkWrongCredentials();
		br.click("xpath=//*[@id='Header_GlobalLogin_WC_AccountDisplay_links_3']/span");//clicking on register link
		
		//Filling Register Page
		br.sendKeys("xpath=//*[@id='WC_UserRegistrationAddForm_FormInput_logonId_In_Register_1_1']", "");
		
  }
  
  public void chkWrongCredentials()throws Exception
  {
	  try{
		//INvalid Credentials
		  String strCred = "Either the logon ID or the password entered is incorrect. Enter the information again.";
		  br.sendKeys("xpath=//*[@id='Header_GlobalLogin_WC_AccountDisplay_FormInput_logonId_In_Logon_1']", "mahesh.nvs@royalcyber.com");
		  br.sendKeys("xpath=//*[@id='Header_GlobalLogin_WC_AccountDisplay_FormInput_logonPassword_In_Logon_1']", "testing1234");
		  br.click("xpath=//*[@id='Header_GlobalLogin_WC_AccountDisplay_links_2']");
		  
			//		  if(br.isDisplayed("xpath=//*[@id='Header_GlobalLogin_logonErrorMessage_GL']"))
			//		  {
			//			  String actualCred = br.getText("xpath=//*[@id='Header_GlobalLogin_logonErrorMessage_GL']");
			//			  
			//			  if(strCred.equals(actualCred))
			//			  {
			//				  System.out.println("Checking Login Credentials message is displayed, as credentials are not Correct");
			//			  }
			//			 
			//		  }
		 System.out.println("checking Login Credentials message is Not displayed. As credentials are CORRECT");
	  }catch (InterruptedException e) {
			log.error(e);
		}
	  
	 Thread.sleep(3000);
	  if(br.isDisplayed("xpath=//*[@id='Header_GlobalLogin_loggedInDropdown_SignOut']/span"))
	  {
		  System.out.println("Sign Out Link is Displayed.Clicking on it");
		  br.click("xpath=//*[@id='Header_GlobalLogin_loggedInDropdown_SignOut']/span");  
	  }
	  else
	  {
		  System.out.println("Sign Out Link NOT Displayed");
	  }
	  
	  
	  //INvalid Username No Password
//		  String strErrorMsg_pwd = "Please type a valid password";
//		  br.sendKeys("xpath=//*[@id='Header_GlobalLogin_WC_AccountDisplay_FormInput_logonId_In_Logon_1']", "test");
//		  br.sendKeys("xpath=//*[@id='Header_GlobalLogin_WC_AccountDisplay_FormInput_logonPassword_In_Logon_1']", "");
//		  br.click("xpath=//*[@id='Header_GlobalLogin_WC_AccountDisplay_links_2']");
//		  String actualMsg=pwd = "";
//		  if(strErrorMsg_pwd.equals(actualMsg=pwd))
//		  {
//			  System.out.println("Checking Login without Password in Giving Proper Error Message");
//		  }
//		  else
//		  {
//			  System.out.println("checking Login without Password is not Giving proper Error Message");
//		  }
	  
	  //INvalid Password No Username
//		  String strErrorMsg_Uname = "Please type a valid Login ID";
//		  br.sendKeys("xpath=//*[@id='Header_GlobalLogin_WC_AccountDisplay_FormInput_logonId_In_Logon_1']", "");
//		  br.sendKeys("xpath=//*[@id='Header_GlobalLogin_WC_AccountDisplay_FormInput_logonPassword_In_Logon_1']", "test");
//		  br.click("xpath=//*[@id='Header_GlobalLogin_WC_AccountDisplay_links_2']");
//		  String actualMsg_Uname = "";
//		  if(strErrorMsg_Uname.equals(actualMsg_Uname))
//		  {
//			  System.out.println("Checking Login without UName in Giving Proper Error Message");
//		  }
//		  else
//		  {
//			  System.out.println("checking Login without UName is not Giving proper Error Message");
//		  }
	  
	  //Invalid data in username and password fields
	  
	 // String strErrorMsg_Uname = "Please type a valid Login ID";
//	  br.sendKeys("xpath=//*[@id='Header_GlobalLogin_WC_AccountDisplay_FormInput_logonId_In_Logon_1']", "#$%^&*");
//	  br.sendKeys("xpath=//*[@id='Header_GlobalLogin_WC_AccountDisplay_FormInput_logonPassword_In_Logon_1']", "#$%^&*");
//	  br.click("xpath=//*[@id='Header_GlobalLogin_WC_AccountDisplay_links_2']");
//	  String actualMsg_Uname = "";
//	  if(strErrorMsg_Uname.equals(actualMsg_Uname))
//	  {
//		  System.out.println("Checking Login without UName in Giving Proper Error Message");
//	  }
//	  else
//	  {
//		  System.out.println("checking Login without UName is not Giving proper Error Message");
//	  }
	  
	  //Ui validations
	  
	  
  
  }

  public void chkProductDetails()throws Exception
  {
	  
  }

}
