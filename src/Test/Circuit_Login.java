package Test;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class Circuit_Login {
	
	@Parameters({"browser"})
	@Test
	public void chkLogin()throws Exception
	{
		String URL = CommonUtils.readFromConfig("BaseURL");
		Browser br = new Browser();
		br.go(URL);	    
		String appTitle = br.getTitle();
		System.out.println("Application title is : "+appTitle);
	}

}
